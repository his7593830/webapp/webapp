import { MenuItem } from "primeng/api/menuitem";

export class SidebarTitle{

  items: MenuItem[] =  [
    {
        "label": "文章分類",
        "items": [
            {
                "label": ".Net (199)",
                "icon": "pi pi-caret-right",
                "url": "http://blog.miniasp.com/category/feed/Net.aspx"
            },
            {
                "label": "Accessibility (3)",
                "icon": "pi pi-caret-right",
                "url": "http://blog.miniasp.com/category/feed/Accessibility.aspx"
            },
            {
                "label": "AngularJS (7)",
                "icon": "pi pi-caret-right",
                "url": "http://blog.miniasp.com/category/feed/AngularJS.aspx"
            },
            {
                "label": "Accessibility (3)",
                "icon": "pi pi-caret-right",
                "url": "http://blog.miniasp.com/category/feed/ASPNET.aspx"
            }
        ]
    }
  ]

}
